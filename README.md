# Tetris

An implementation of Tetris using SDL2 and OpenGL in Rust.

Requires [sdl2](https://www.libsdl.org) library to be available.

Build and run with `cargo run`.
