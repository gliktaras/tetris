extern crate gl;

use self::gl::types::*;
use std::cell::RefCell;
use std::collections::HashMap;
use std::ffi::CString;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use std::ptr;

/// A convenience wrapper around a shader program.
pub struct ShaderProgram {
    handle: GLuint,
    attributes: RefCell<HashMap<String, GLint>>,
    uniforms: RefCell<HashMap<String, GLint>>,
}

impl ShaderProgram {
    /// Returns a new wrapper that uses the provided shader program handle.
    pub fn new(handle: GLuint) -> ShaderProgram {
        ShaderProgram {
            handle: handle,
            attributes: RefCell::new(HashMap::new()),
            uniforms: RefCell::new(HashMap::new()),
        }
    }

    /// Returns the location of an attribute with the provided name, if it exists.
    pub unsafe fn attribute_location(&self, name: &str) -> Option<GLuint> {
        let get_location_fn = || {
            gl::GetAttribLocation(self.handle,
                                  CString::new(name).unwrap().as_ptr() as *const GLchar)
        };
        let location = *(*self.attributes.borrow_mut())
            .entry(String::from(name))
            .or_insert_with(get_location_fn);

        if location >= 0 {
            Some(location as GLuint)
        } else {
            None
        }
    }

    /// Returns the location of a uniform with the provided name, if it exists.
    pub unsafe fn uniform_location(&self, name: &str) -> Option<GLint> {
        let get_location_fn = || {
            gl::GetUniformLocation(self.handle,
                                   CString::new(name).unwrap().as_ptr() as *const GLchar)
        };
        let location = *(*self.uniforms.borrow_mut())
            .entry(String::from(name))
            .or_insert_with(get_location_fn);
        if location >= 0 { Some(location) } else { None }
    }

    /// Uses the shader program.
    pub unsafe fn use_program(&self) {
        gl::UseProgram(self.handle);
    }
}

/// Shader related errors.
#[derive(Debug)]
pub enum ShaderError {
    CannotCreateShader(GLenum),
    CannotCreateProgram,
    FailedToCompile(GLenum, String),
    FailedToLink(String),
}

/// Attempts to convert the name of a shader file into a shader map key. Expects shader file name
/// to have the form "$SHADER_NAME.$SHADER_TYPE.glsl".
fn shader_file_name_to_key(file_name: &str) -> Option<(GLenum, String)> {
    let parts: Vec<&str> = file_name.split(".").collect();
    if parts.len() != 3 || parts[2] != "glsl" {
        return None;
    }
    let name = parts[0];
    let type_ = match parts[1] {
        "vert" => gl::VERTEX_SHADER,
        "frag" => gl::FRAGMENT_SHADER,
        _ => {
            return None;
        }
    };
    Some((type_, String::from(name)))
}

/// Compiles a shader with the provided `type_` and `source` code and returns the shader's GL
/// handle.
unsafe fn compile_shader(type_: GLenum, source: &str) -> Result<GLuint, ShaderError> {
    let shader = gl::CreateShader(type_);
    if shader == 0 {
        return Err(ShaderError::CannotCreateShader(type_))
    }

    let source_c = CString::new(source).unwrap();
    gl::ShaderSource(shader, 1, &source_c.as_ptr(), ptr::null());
    gl::CompileShader(shader);

    let mut status = gl::FALSE as GLint;
    gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
    if status == (gl::FALSE as GLint) {
        let mut info_log_length = 0;
        gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut info_log_length);

        let mut info_log = Vec::with_capacity(info_log_length as usize);
        info_log.set_len((info_log_length - 1) as usize);
        gl::GetShaderInfoLog(shader,
                             info_log_length,
                             ptr::null_mut(),
                             info_log.as_mut_ptr() as *mut GLchar);
        return Err(ShaderError::FailedToCompile(type_, String::from_utf8(info_log).unwrap()));
    }
    Ok(shader)
}

/// Links a shader program with the provided set of `shaders` and returns the program's GL handle.
unsafe fn link_program(shaders: &[GLuint]) -> Result<GLuint, ShaderError> {
    let program = gl::CreateProgram();
    if program == 0 {
        return Err(ShaderError::CannotCreateProgram);
    }

    for shader in shaders {
        gl::AttachShader(program, *shader);
    }
    gl::LinkProgram(program);

    let mut status = gl::FALSE as GLint;
    gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);
    if status == (gl::FALSE as GLint) {
        let mut info_log_length = 0;
        gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut info_log_length);

        let mut info_log = Vec::with_capacity(info_log_length as usize);
        info_log.set_len((info_log_length - 1) as usize);
        gl::GetProgramInfoLog(program,
                              info_log_length,
                              ptr::null_mut(),
                              info_log.as_mut_ptr() as *mut GLchar);
        return Err(ShaderError::FailedToLink(String::from_utf8(info_log).unwrap()));
    }
    Ok(program)
}

/// Shader's source code and GL handle.
struct ShaderEntry {
    source: String,
    handle: GLuint,
}

/// Provides easy access to GLSL shaders that are defined within `data/shaders` directory.
pub struct ShaderProvider {
    shaders: HashMap<(GLenum, String), ShaderEntry>,
}

impl ShaderProvider {
    /// Creates a new shader provider to shaders in `shader_dir` directory.
    pub fn new(shader_dir: &Path) -> Result<ShaderProvider, io::Error> {
        let mut shaders = HashMap::new();
        for entry in try!(fs::read_dir(shader_dir)) {
            let path = try!(entry).path();
            if !path.is_file() {
                continue;
            }

            let key;
            {
                let file_name = path.file_name().unwrap().to_str().unwrap();
                key = shader_file_name_to_key(file_name);
                if key.is_none() {
                    warn!("Found shader file with malformed name: {}",
                          path.to_str().unwrap());
                    continue;
                }
            }
            let key = key.unwrap();

            let mut file = try!(File::open(path));
            let mut source = String::new();
            file.read_to_string(&mut source).unwrap();

            shaders.insert(key,
                           ShaderEntry {
                               source: source,
                               handle: 0,
                           });
        }

        Ok(ShaderProvider { shaders: shaders })
    }

    /// Returns the GL handle of a shader with the provided `type_` and `name`. If needed, compiles
    /// the shader first.
    unsafe fn get_or_compile_shader(&mut self,
                                    type_: GLenum,
                                    name: &str)
                                    -> Result<GLuint, ShaderError> {
        let shader = self.shaders
            .get_mut(&(type_, String::from(name)))
            .expect(&format!("Shader \"{}\" of type {} does not exist.", name, type_));
        if shader.handle == 0 {
            let handle = try!(compile_shader(type_, &shader.source));
            shader.handle = handle;
        }
        Ok(shader.handle)
    }

    /// Returns the GL handle of a shader program that consists of vertex and fragment shaders with
    /// the provided names.
    pub unsafe fn new_program(&mut self,
                              vertex_shader_name: &str,
                              fragment_shader_name: &str)
                              -> Result<ShaderProgram, ShaderError> {
        let vertex_shader_handle =
            try!(self.get_or_compile_shader(gl::VERTEX_SHADER, vertex_shader_name));
        let fragment_shader_handle =
            try!(self.get_or_compile_shader(gl::FRAGMENT_SHADER, fragment_shader_name));
        link_program(&[vertex_shader_handle, fragment_shader_handle])
            .map(|handle| ShaderProgram::new(handle))
    }
}
