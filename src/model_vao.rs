#![allow(dead_code)]

extern crate gl;
extern crate tobj;

use self::gl::types::*;
use std::mem;
use std::path::Path;

/// ModelVAO error values.
#[derive(Debug)]
pub enum ModelVAOError {
    LoadError(tobj::LoadError),
    MultipleModels,
}

/// A vertex array object that contains all data necessary to render a single model.
#[derive(Clone)]
pub struct ModelVAO {
    vao: GLuint,
    name: Option<String>,
    vertex_coord_offset: usize,
    normal_offset: usize,
    texture_coord_offset: usize,
    index_count: usize,
}

impl ModelVAO {
    /// Creates a new model with the provided vertex coordinates, vertex normals, texture
    /// coordinates, element indices and, optionally, name. Mesh data is represented as a flat
    /// sequence of values, and indices must be usable with GL_TRIANGLES drawing mode.
    pub unsafe fn new(vertex_coords: &Vec<f32>,
                      normals: &Vec<f32>,
                      texture_coords: &Vec<f32>,
                      indices: &Vec<u32>,
                      name: Option<String>)
                      -> ModelVAO {
        let mut vao = 0;
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);

        let mut array_vbo = 0;
        gl::GenBuffers(1, &mut array_vbo);
        gl::BindBuffer(gl::ARRAY_BUFFER, array_vbo);

        let array_vbo_size = (vertex_coords.len() + normals.len() + texture_coords.len()) *
                             mem::size_of::<f32>();
        gl::BufferData(gl::ARRAY_BUFFER,
                       array_vbo_size as GLsizeiptr,
                       0 as *const GLvoid,
                       gl::STATIC_DRAW);

        let vertex_coord_offset = 0;
        gl::BufferSubData(gl::ARRAY_BUFFER,
                          vertex_coord_offset as GLintptr,
                          (vertex_coords.len() * mem::size_of::<f32>()) as GLsizeiptr,
                          vertex_coords.as_ptr() as *const GLvoid);

        let normal_offset = vertex_coord_offset + vertex_coords.len() * mem::size_of::<f32>();
        gl::BufferSubData(gl::ARRAY_BUFFER,
                          normal_offset as GLintptr,
                          (normals.len() * mem::size_of::<f32>()) as GLsizeiptr,
                          normals.as_ptr() as *const GLvoid);

        let texture_coord_offset = normal_offset + normals.len() * mem::size_of::<f32>();
        gl::BufferSubData(gl::ARRAY_BUFFER,
                          texture_coord_offset as GLintptr,
                          (texture_coords.len() * mem::size_of::<f32>()) as GLsizeiptr,
                          texture_coords.as_ptr() as *const GLvoid);

        let mut element_vbo = 0;
        gl::GenBuffers(1, &mut element_vbo);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, element_vbo);
        gl::BufferData(gl::ELEMENT_ARRAY_BUFFER,
                       (indices.len() * mem::size_of::<u32>()) as GLsizeiptr,
                       indices.as_ptr() as *const GLvoid,
                       gl::STATIC_DRAW);

        gl::BindVertexArray(0);
        ModelVAO {
            vao: vao,
            name: name,
            vertex_coord_offset: vertex_coord_offset,
            normal_offset: normal_offset,
            texture_coord_offset: texture_coord_offset,
            index_count: indices.len(),
        }
    }

    /// Creates model from an obj file with the provided path. Expects the file to contain a single
    /// model.
    pub unsafe fn from_obj(model_path: &Path) -> Result<ModelVAO, ModelVAOError> {
        let (models, _) = match tobj::load_obj(model_path) {
            Ok(value) => value,
            Err(err) => {
                return Err(ModelVAOError::LoadError(err));
            }
        };
        if models.len() > 1 {
            return Err(ModelVAOError::MultipleModels);
        }

        let model = &models[0];
        let name = if !model.name.is_empty() {
            Some(model.name.clone())
        } else {
            None
        };
        Ok(ModelVAO::new(&model.mesh.positions,
                         &model.mesh.normals,
                         &model.mesh.texcoords,
                         &model.mesh.indices,
                         name))
    }

    /// Returns the name of the model.
    pub fn name(&self) -> Option<String> {
        self.name.clone()
    }

    /// Binds the model's VAO.
    pub unsafe fn bind_vao(&self) {
        gl::BindVertexArray(self.vao);
    }

    /// Draws the model. Assumes that its VAO was bound.
    pub unsafe fn draw(&self) {
        gl::DrawElements(gl::TRIANGLES,
                         self.index_count as GLint,
                         gl::UNSIGNED_INT,
                         0 as *const GLvoid);
    }

    /// Defines the data of a shader attribute at the given `location`, to have `dims` dimensions
    /// and start `offset` bytes in the array data VBO.
    unsafe fn define_attribute(&self, location: GLuint, dims: i32, offset: usize) {
        gl::VertexAttribPointer(location,
                                dims as GLint,
                                gl::FLOAT,
                                gl::FALSE,
                                0,
                                offset as *const GLvoid);
    }

    /// Defines the shader attribute at the given `location` to have `dims` dimensional vertex
    /// coordinate data.
    pub unsafe fn define_vertex_coord_attribute(&self, location: GLuint, dims: i32) {
        self.define_attribute(location, dims, self.vertex_coord_offset);
    }

    /// Defines the shader attribute at the given `location` to have `dims` dimensional normal
    /// data.
    pub unsafe fn define_normal_attribute(&self, location: GLuint, dims: i32) {
        self.define_attribute(location, dims, self.normal_offset);
    }

    /// Defines the shader attribute at the given `location` to have `dims` dimensional texture
    /// coordinate data.
    pub unsafe fn define_texture_coord_attribute(&self, location: GLuint, dims: i32) {
        self.define_attribute(location, dims, self.texture_coord_offset);
    }
}
