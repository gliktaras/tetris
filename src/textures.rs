#![allow(dead_code)]

extern crate gl;
extern crate sdl2;

use self::gl::types::*;
use self::sdl2::pixels::Color as SdlColor;
use self::sdl2::ttf::{Font, FontError};

/// A container for a 1D bitmap texture.
pub struct BitmapTexture {
    texture: GLuint,
    width: u32,
    height: u32,
    aspect_ratio: f32,
}

impl BitmapTexture {
    /// Returns a new bitmap texture.
    pub unsafe fn new() -> BitmapTexture {
        let mut texture = 0;
        gl::GenTextures(1, &mut texture);

        gl::BindTexture(gl::TEXTURE_2D, texture);
        gl::TexParameteri(gl::TEXTURE_2D,
                          gl::TEXTURE_WRAP_S,
                          gl::CLAMP_TO_EDGE as GLint);
        gl::TexParameteri(gl::TEXTURE_2D,
                          gl::TEXTURE_WRAP_T,
                          gl::CLAMP_TO_EDGE as GLint);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as GLint);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as GLint);
        gl::BindTexture(gl::TEXTURE_2D, 0);

        BitmapTexture {
            texture: texture,
            width: 0,
            height: 0,
            aspect_ratio: 1.0,
        }
    }

    /// Returns the bitmap's width in pixels.
    pub fn width(&self) -> u32 {
        self.width
    }

    /// Returns the bitmap's height in pixels.
    pub fn height(&self) -> u32 {
        self.height
    }

    /// Returns the ratio of the bitmap's width to its height.
    pub fn aspect_ratio(&self) -> f32 {
        self.aspect_ratio
    }

    /// Binds the texture to `GL_TEXTURE_2D` target.
    pub unsafe fn bind(&self) {
        gl::BindTexture(gl::TEXTURE_2D, self.texture);
    }

    /// Sets the contents of the bitmap to be bytes in `data` with dimensions `width` bytes by
    /// `height`.
    pub unsafe fn set_contents(&mut self, width: u32, height: u32, data: &[u8]) {
        self.bind();
        gl::TexImage2D(gl::TEXTURE_2D,
                       0,
                       1,
                       width as GLint,
                       height as GLint,
                       0,
                       gl::RED,
                       gl::UNSIGNED_BYTE,
                       data.as_ptr() as *const GLvoid);
        self.width = width;
        self.height = height;
        self.aspect_ratio = width as f32 / height as f32;
        gl::BindTexture(gl::TEXTURE_2D, 0);
    }
}


/// Text texture's error values.
#[derive(Debug)]
pub enum TextTextureError {
    CannotRenderText(FontError),
}

/// A container for a texture that contains text.
pub struct TextTexture {
    texture: BitmapTexture,
}

impl TextTexture {
    /// Returns a new text texture.
    pub unsafe fn new() -> TextTexture {
        TextTexture { texture: BitmapTexture::new() }
    }

    /// Returns a new text texture, which contains `text` in the provided `font`.
    pub unsafe fn new_with_contents(font: &Font,
                                    text: &str)
                                    -> Result<TextTexture, TextTextureError> {
        let mut texture = TextTexture::new();
        try!(texture.set_contents(font, text));
        Ok(texture)
    }

    /// Returns the texture's width in pixels.
    pub fn width(&self) -> u32 {
        self.texture.width()
    }

    /// Returns the texture's height in pixels.
    pub fn height(&self) -> u32 {
        self.texture.height()
    }

    /// Returns the ratio of the texture's width to its height.
    pub fn aspect_ratio(&self) -> f32 {
        self.texture.aspect_ratio()
    }

    /// Binds the texture to `GL_TEXTURE_2D` target.
    pub unsafe fn bind(&self) {
        self.texture.bind();
    }

    /// Sets the contents of the texture to be `text` in the provided `font`.
    pub unsafe fn set_contents(&mut self, font: &Font, text: &str) -> Result<(), TextTextureError> {
        let surface = font.render(text)
            .shaded(SdlColor::RGB(0, 0, 0), SdlColor::RGB(255, 255, 255));
        let surface = match surface {
            Ok(value) => value,
            Err(err) => {
                return Err(TextTextureError::CannotRenderText(err));
            }
        };
        surface.with_lock(|data| {
            let height = surface.height();
            let width = data.len() as u32 / height;
            self.texture.set_contents(width, height, data);
        });

        Ok(())
    }
}
