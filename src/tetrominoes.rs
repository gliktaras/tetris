#![allow(dead_code)]

extern crate rand;

use bitmap::Bitmap;

/// Possible playfield block colors.
#[derive(Clone, Eq, Hash, PartialEq)]
pub enum BlockColor {
    Blue, // J piece color
    Cyan, // I piece color
    Green, // S piece color
    Orange, // L piece color
    Purple, // T piece color
    Red, // Z piece color
    White, // Neutral
    Yellow, // O piece color
}

impl Default for BlockColor {
    fn default() -> BlockColor {
        BlockColor::White
    }
}

/// Possible tetromino kinds.
pub enum TetrominoKind {
    I = 0,
    J = 1,
    L = 2,
    O = 3,
    S = 4,
    T = 5,
    Z = 6,
}

/// Factory that produces new tetrominoes.
pub struct TetrominoFactory {
    tetrominoes: Vec<Bitmap<BlockColor>>,
    bag: Vec<usize>,
    bag_index: usize,
}

/// Creates a new square bitmap whose dimensions are `size` by `size` using the provided bits and
/// color. `bits` string must list bitmap's bit using characters `'0'` and `'1'` in row major
/// order. The provided `color` will be set for all blocks.
fn create_tetromino_bitmap(size: usize, bits: &str, color: &BlockColor) -> Bitmap<BlockColor> {
    let bits = bits.as_bytes();
    assert_eq!(bits.len(), size * size, "Invalid bit count.");

    let mut bitmap: Bitmap<BlockColor> = Bitmap::new(size, size);
    let mut i = 0;
    for y in 0..size as i32 {
        for x in 0..size as i32 {
            bitmap.set_entry_parts(x, y, bits[i] == '1' as u8, color.clone());
            i += 1;
        }
    }
    bitmap
}

impl TetrominoFactory {
    /// Returns a new factory.
    pub fn new() -> TetrominoFactory {
        let piece_i = create_tetromino_bitmap(4, "0000000011110000", &BlockColor::Cyan);
        let piece_j = create_tetromino_bitmap(3, "001111000", &BlockColor::Blue);
        let piece_l = create_tetromino_bitmap(3, "100111000", &BlockColor::Orange);
        let piece_o = create_tetromino_bitmap(2, "1111", &BlockColor::Yellow);
        let piece_s = create_tetromino_bitmap(3, "000110011", &BlockColor::Green);
        let piece_t = create_tetromino_bitmap(3, "000111010", &BlockColor::Purple);
        let piece_z = create_tetromino_bitmap(3, "000011110", &BlockColor::Red);

        // Tetromino location in this vector must match the values of the TetrominoKind enum.
        let tetrominoes = vec![piece_i, piece_j, piece_l, piece_o, piece_s, piece_t, piece_z];
        let bag = (0..tetrominoes.len()).collect();
        let bag_index = tetrominoes.len();

        TetrominoFactory {
            tetrominoes: tetrominoes,
            bag: bag,
            bag_index: bag_index,
        }
    }

    /// Resets the factory for a new game.
    pub fn reset<R: rand::Rng>(&mut self, rng: &mut R) {
        rng.shuffle(self.bag.as_mut_slice());
        self.bag_index = 0;
    }

    /// Returns the next tetromino in the randomized sequence. Uses the Random Generator algorithm
    /// for selecting the order of the pieces.
    pub fn next_piece<R: rand::Rng>(&mut self, rng: &mut R) -> Bitmap<BlockColor> {
        if self.bag_index >= self.bag.len() {
            self.reset(rng);
        }
        let piece = self.tetrominoes[self.bag[self.bag_index]].clone();
        self.bag_index += 1;
        piece
    }

    /// Returns a new particular tetromino.
    pub fn specific_piece(&self, kind: TetrominoKind) -> Bitmap<BlockColor> {
        self.tetrominoes[kind as usize].clone()
    }
}
