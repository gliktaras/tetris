extern crate sdl2;

use self::sdl2::event::Event;
use self::sdl2::keyboard::Keycode;
use std::collections::HashMap;
use std::collections::HashSet;

/// Groups of commands that can be bound to a single input.
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum CommandGroup {
    HardDrop,
    Hold,
    MoveLeft,
    MoveRight,
    RotateClockwise,
    RotateCounterClockwise,
    SoftDrop,
    Start,
}

/// Individual commands that can affect the game's state.
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum Command {
    HardDrop,
    Hold,
    MoveLeftOff,
    MoveLeftOn,
    MoveRightOff,
    MoveRightOn,
    RotateClockwise,
    RotateCounterClockwise,
    SoftDropOff,
    SoftDropOn,
    Start,
}

/// Input interpreter specific errors.
#[derive(Debug)]
pub enum InputInterpreterError {
    InvalidKeycodeName(String),
    MissingBindings(Vec<Command>),
}

/// Converts a keycode `name` to the corresponding keycode and returns it. Returns an error if the
/// provided `name` is invalid.
fn keycode_from_name(name: &str) -> Result<Keycode, InputInterpreterError> {
    match Keycode::from_name(name) {
        Some(keycode) => Ok(keycode),
        None => Err(InputInterpreterError::InvalidKeycodeName(String::from(name))),
    }
}

/// Converts group bindings with keycode names to group bindings with keycodes.
fn convert_keys_to_keycodes(str_bindings: HashMap<CommandGroup, &str>)
                            -> Result<HashMap<CommandGroup, Keycode>, InputInterpreterError> {
    let mut keycode_bindings = HashMap::new();
    for (group, keycode_name) in str_bindings {
        keycode_bindings.insert(group.clone(), try!(keycode_from_name(keycode_name)));
    }
    Ok(keycode_bindings)
}

/// Checks that `keycode_map` contains bindings for all commands in `expected`.
fn check_commands_bound(keycode_map: &HashMap<Keycode, Command>,
                        expected: &[Command])
                        -> Result<(), InputInterpreterError> {
    let expected_set: HashSet<Command> = expected.iter().cloned().collect();
    let bound_set: HashSet<Command> = keycode_map.values().cloned().collect();
    let missing: Vec<Command> = expected_set.difference(&bound_set).cloned().collect();
    if missing.len() > 0 {
        return Err(InputInterpreterError::MissingBindings(missing));
    }
    Ok(())
}

/// Creates key down bindings from the provided `group_bindings` and returns them.
fn new_key_down_bindings(group_bindings: &HashMap<CommandGroup, Keycode>)
                         -> Result<HashMap<Keycode, Command>, InputInterpreterError> {
    let mut key_down_bindings = HashMap::new();
    for (group, keycode) in group_bindings {
        let target = match *group {
            CommandGroup::Start => Some(Command::Start),
            CommandGroup::HardDrop => Some(Command::HardDrop),
            CommandGroup::Hold => Some(Command::Hold),
            CommandGroup::SoftDrop => Some(Command::SoftDropOn),
            CommandGroup::MoveLeft => Some(Command::MoveLeftOn),
            CommandGroup::MoveRight => Some(Command::MoveRightOn),
            CommandGroup::RotateClockwise => Some(Command::RotateClockwise),
            CommandGroup::RotateCounterClockwise => Some(Command::RotateCounterClockwise),
        };
        target.map(|command| { key_down_bindings.insert(*keycode, command); });
    }

    const KEY_DOWN_COMMANDS: [Command; 8] = [Command::Start,
                                             Command::HardDrop,
                                             Command::Hold,
                                             Command::SoftDropOn,
                                             Command::MoveLeftOn,
                                             Command::MoveRightOn,
                                             Command::RotateClockwise,
                                             Command::RotateCounterClockwise];
    try!(check_commands_bound(&key_down_bindings, &KEY_DOWN_COMMANDS));
    Ok(key_down_bindings)
}

/// Creates new key up bindings from the provided `group_bindings` and returns them.
fn new_key_up_bindings(group_bindings: &HashMap<CommandGroup, Keycode>)
                       -> Result<HashMap<Keycode, Command>, InputInterpreterError> {
    let mut key_up_bindings: HashMap<Keycode, Command> = HashMap::new();
    for (group, keycode) in group_bindings {
        let target = match *group {
            CommandGroup::SoftDrop => Some(Command::SoftDropOff),
            CommandGroup::MoveLeft => Some(Command::MoveLeftOff),
            CommandGroup::MoveRight => Some(Command::MoveRightOff),
            _ => None,
        };
        target.map(|command| { key_up_bindings.insert(*keycode, command); });
    }

    const KEY_UP_COMMANDS: [Command; 3] =
        [Command::SoftDropOff, Command::MoveLeftOff, Command::MoveRightOff];
    try!(check_commands_bound(&key_up_bindings, &KEY_UP_COMMANDS));
    Ok(key_up_bindings)
}

/// Converts input events to commands.
pub struct InputInterpreter {
    group_bindings: HashMap<CommandGroup, Keycode>,
    key_down_bindings: HashMap<Keycode, Command>,
    key_up_bindings: HashMap<Keycode, Command>,
    commands: Vec<Command>,
}

impl InputInterpreter {
    /// Creates a new input interpreter from the provided `group_bindings`.
    pub fn new(group_bindings: HashMap<CommandGroup, &str>)
               -> Result<InputInterpreter, InputInterpreterError> {
        let group_bindings = try!(convert_keys_to_keycodes(group_bindings));
        let key_down_bindings = try!(new_key_down_bindings(&group_bindings));
        let key_up_bindings = try!(new_key_up_bindings(&group_bindings));
        Ok(InputInterpreter {
               group_bindings: group_bindings,
               key_down_bindings: key_down_bindings,
               key_up_bindings: key_up_bindings,
               commands: vec![],
           })
    }

    /// Returns the bound keycode for the provided command `group`.
    pub fn bound_keycode(&self, group: &CommandGroup) -> Option<&Keycode> {
        self.group_bindings.get(group)
    }

    /// Records the provided `event`. Returns if it was recognized and processed.
    pub fn record_event(&mut self, event: &Event) -> bool {
        let command = match *event {
            Event::KeyDown { keycode, .. } => self.key_down_bindings.get(&keycode.unwrap()),
            Event::KeyUp { keycode, .. } => self.key_up_bindings.get(&keycode.unwrap()),
            _ => None,
        };
        if command.is_some() {
            self.commands.push(command.unwrap().clone());
            return true;
        }
        false
    }

    /// Returns the accumulated commands and clears the command buffer.
    pub fn get_commands_and_advance(&mut self) -> Vec<Command> {
        let current_commands = self.commands.clone();
        self.commands.clear();
        current_commands
    }
}
