extern crate font_loader;
extern crate sdl2;

use self::font_loader::system_fonts;
use self::sdl2::rwops::RWops;
use self::sdl2::ttf::{Font, Sdl2TtfContext};
use std::collections::HashMap;

/// Font provider's error values.
#[derive(Debug)]
pub enum FontProviderError {
    CannotLoadSystemFont(String),
    FontFamilyNotLoaded(String),
    Sdl2RWopsError(String),
    Sdl2FontError(String),
}

/// Loads and constructs SDL2 fonts.
pub struct FontProvider {
    sdl_ttf_context: Sdl2TtfContext,
    ttf_cache: HashMap<String, Box<Vec<u8>>>,
}

impl FontProvider {
    /// Returns a new font provider that uses the provided TTF context for font creation.
    pub fn new(sdl_ttf_context: Sdl2TtfContext) -> FontProvider {
        FontProvider {
            sdl_ttf_context: sdl_ttf_context,
            ttf_cache: HashMap::new(),
        }
    }

    /// Attempts to load a font family with `family_name`.
    pub fn load_family(&mut self, family_name: &str) -> Result<(), FontProviderError> {
        if self.ttf_cache.contains_key(family_name) {
            return Ok(());
        }

        let property = system_fonts::FontPropertyBuilder::new().family(family_name).build();
        let (ttf_bytes, _) = match system_fonts::get(&property) {
            Some(value) => value,
            None => {
                return Err(FontProviderError::CannotLoadSystemFont(String::from(family_name)));
            }
        };
        self.ttf_cache.insert(String::from(family_name), Box::new(ttf_bytes));
        Ok(())
    }

    /// Creates and returns a new font with the provided `family_name` and `point_size`.
    pub fn get_font<'a>(&'a self,
                        family_name: &str,
                        point_size: u16)
                        -> Result<Font<'a>, FontProviderError> {
        let ttf_bytes = match self.ttf_cache.get(family_name) {
            Some(value) => value,
            None => {
                return Err(FontProviderError::FontFamilyNotLoaded(String::from(family_name)));
            }
        };
        let rwops = match RWops::from_bytes(ttf_bytes) {
            Ok(value) => value,
            Err(err) => {
                return Err(FontProviderError::Sdl2RWopsError(err));
            }
        };
        let font = match self.sdl_ttf_context.load_font_from_rwops(rwops, point_size) {
            Ok(value) => value,
            Err(err) => {
                return Err(FontProviderError::Sdl2FontError(err));
            }
        };
        Ok(font)
    }
}
