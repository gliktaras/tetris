extern crate gl;

use self::gl::types::*;
use std::collections::HashMap;
use tetrominoes::BlockColor;

/// An RGBA color.
#[derive(Clone)]
pub struct RgbaColor {
    rgba: [f32; 4],
}

impl RgbaColor {
    /// Returns a new color with the provided component values.
    pub fn new(r: f32, g: f32, b: f32, a: f32) -> RgbaColor {
        RgbaColor { rgba: [r, g, b, a] }
    }

    /// Returns the color with its alpha component to `a`.
    pub fn with_alpha(&self, a: f32) -> RgbaColor {
        RgbaColor { rgba: [self.rgba[0], self.rgba[1], self.rgba[2], a], }
    }

    /// Returns the color as pointer, suitable for use with OpenGL's `glUniform*fv` functions.
    pub fn as_gl_ptr(&self) -> *const GLfloat {
        self.rgba.as_ptr() as *const GLfloat
    }
}

/// Provides colors.
pub struct ColorProvider {
    block_colors: HashMap<BlockColor, RgbaColor>,
    shadow_colors: HashMap<BlockColor, RgbaColor>,
    black: RgbaColor,
    white: RgbaColor,
    playfield_background: RgbaColor,
}

impl ColorProvider {
    /// Returns a new color provider.
    pub fn new() -> ColorProvider {
        let mut block_colors = HashMap::new();
        block_colors.insert(BlockColor::Blue, RgbaColor::new(0.0, 0.0, 1.0, 1.0));
        block_colors.insert(BlockColor::Cyan, RgbaColor::new(0.0, 1.0, 1.0, 1.0));
        block_colors.insert(BlockColor::Green, RgbaColor::new(0.0, 1.0, 0.0, 1.0));
        block_colors.insert(BlockColor::Orange, RgbaColor::new(1.0, 0.5, 0.5, 1.0));
        block_colors.insert(BlockColor::Purple, RgbaColor::new(1.0, 0.0, 1.0, 1.0));
        block_colors.insert(BlockColor::Red, RgbaColor::new(1.0, 0.0, 0.0, 1.0));
        block_colors.insert(BlockColor::White, RgbaColor::new(1.0, 1.0, 1.0, 1.0));
        block_colors.insert(BlockColor::Yellow, RgbaColor::new(1.0, 1.0, 0.0, 1.0));

        let mut shadow_colors = HashMap::new();
        for (key, color) in &block_colors {
            shadow_colors.insert(key.clone(), color.with_alpha(0.5));
        }

        ColorProvider {
            block_colors: block_colors,
            shadow_colors: shadow_colors,
            black: RgbaColor::new(0.0, 0.0, 0.0, 1.0),
            white: RgbaColor::new(1.0, 1.0, 1.0, 1.0),
            playfield_background: RgbaColor::new(0.2, 0.2, 0.2, 1.0),
        }
    }

    /// Returns the RGBA color of a tetromino block of the provided `color`.
    pub fn block_color(&self, color: &BlockColor) -> &RgbaColor {
        self.block_colors.get(&color).unwrap()
    }

    /// Returns the RGBA color of a shadow tetromino block of the provided `color`.
    pub fn shadow_color(&self, color: &BlockColor) -> &RgbaColor {
        self.shadow_colors.get(&color).unwrap()
    }

    /// Returns RGBA of the color black.
    pub fn black(&self) -> &RgbaColor {
        &self.black
    }

    /// Returns RGBA of the color white.
    pub fn white(&self) -> &RgbaColor {
        &self.white
    }

    /// Returns RGBA of the playfield background.
    pub fn playfield_background(&self) -> &RgbaColor {
        &self.playfield_background
    }
}
