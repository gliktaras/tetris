#![allow(dead_code)]

use std::collections::HashSet;

/// Possible bitmap rotations.
#[derive(Clone)]
enum Rotation {
    By0Degrees,
    By90Degrees,
    By180Degrees,
    By270Degrees,
}

bitflags! {
    /// Bit query operation results.
    pub flags BitQueryResult: u8 {
        const NONE_SET           = 0b00000001,
        const BASE_SET           = 0b00000010,
        const MASK_SET           = 0b00000100,
        const BASE_AND_MASK_SET  = 0b00001000,
        const MASK_UNSET_OUTSIDE = 0b00010000,
        const MASK_SET_OUTSIDE   = 0b00100000,
    }
}

/// Blit operations.
pub enum BlitOp {
    /// Unsets all set bits of the mask.
    MaskOff,
    /// Sets all set bits of the mask and overwrites data.
    MaskOn,
}

/// Contents of a single bitmap cell.
#[derive(Clone)]
pub struct BitmapEntry<T> {
    /// Entry's bit value.
    pub bit: bool,
    /// Data attached to the entry.
    pub data: T,
}

impl<T> Default for BitmapEntry<T> where T: Default {
    fn default() -> BitmapEntry<T> {
        BitmapEntry { bit: false, data: Default::default() }
    }
}

/// A two-dimensional bitmap that has an instance of `T` attached to every cell. Assumes that
/// coordinates `(0, 0)` are in the lower-left corner.
#[derive(Clone)]
pub struct Bitmap<T> {
    width: usize,
    height: usize,
    entries: Vec<BitmapEntry<T>>,
    rotation: Rotation,
}

impl<T> Bitmap<T>
    where T: Clone + Default
{
    /// Returns a new bitmap with the specified dimensions that has no set bits.
    pub fn new(width: usize, height: usize) -> Bitmap<T> {
        Bitmap {
            width: width,
            height: height,
            entries: vec![Default::default(); width * height],
            rotation: Rotation::By0Degrees,
        }
    }

    /// Returns the width of the bitmap.
    pub fn width(&self) -> usize {
        match self.rotation {
            Rotation::By0Degrees |
            Rotation::By180Degrees => self.width,
            Rotation::By90Degrees |
            Rotation::By270Degrees => self.height,
        }
    }

    /// Returns the height of the bitmap.
    pub fn height(&self) -> usize {
        match self.rotation {
            Rotation::By0Degrees |
            Rotation::By180Degrees => self.height,
            Rotation::By90Degrees |
            Rotation::By270Degrees => self.width,
        }
    }

    /// Returns the index to the `entries` vector, which corresponds to the coordinates `(x, y)`,
    /// if `(x, y)` is within the bitmap's bounds.
    fn index(&self, x: i32, y: i32) -> Option<usize> {
        if x < 0 || x >= self.width() as i32 || y < 0 || y >= self.height() as i32 {
            return None;
        }
        let x = x as usize;
        let y = y as usize;

        let i = match self.rotation {
            Rotation::By0Degrees => x + self.width() * y,
            Rotation::By90Degrees => (self.height() - y - 1) + self.height() * x,
            Rotation::By180Degrees => {
                (self.width() - x - 1) + self.width() * (self.height() - y - 1)
            }
            Rotation::By270Degrees => y + self.height() * (self.width() - x - 1),
        };
        Some(i)
    }

    /// Returns the bitmap entry at coordinates `(x, y)`, if it exists.
    pub fn entry(&self, x: i32, y: i32) -> Option<&BitmapEntry<T>> {
        self.index(x, y).map(|i| &self.entries[i])
    }

    /// Returns the mutable bitmap entry at coordinates `(x, y)`, if it exists.
    pub fn mut_entry(&mut self, x: i32, y: i32) -> Option<&mut BitmapEntry<T>> {
        self.index(x, y).map(move |i| &mut self.entries[i])
    }

    /// Returns the bit value of the entry at coordinates `(x, y)`, if this entry exists.
    pub fn bit(&self, x: i32, y: i32) -> Option<bool> {
        self.entry(x, y).map(|entry| entry.bit)
    }

    /// Sets the bit value of the entry at coordinates `(x, y)` to `new_bit`, if this entry exists.
    pub fn set_bit(&mut self, x: i32, y: i32, new_bit: bool) {
        self.mut_entry(x, y).map(|entry| entry.bit = new_bit);
    }

    /// Returns data, attached to the entry at coordinates `(x, y)`, if this entry exists.
    pub fn data(&self, x: i32, y: i32) -> Option<&T> {
        self.entry(x, y).map(|entry| &entry.data)
    }

    /// Returns a mutable reference to data, attached to the entry at coordinates `(x, y)`, if this
    /// entry exists.
    pub fn mut_data(&mut self, x: i32, y: i32) -> Option<&mut T> {
        self.mut_entry(x, y).map(|entry| &mut entry.data)
    }

    /// Sets the data of the entry at coordinates at `(x, y)`, if it exists.
    pub fn set_data(&mut self, x: i32, y: i32, new_data: T) {
        self.mut_entry(x, y).map(|entry| entry.data = new_data);
    }

    /// Sets the contents of the bitmap entry at coordinates `(x, y)` from parts, if this entry
    /// exists.
    pub fn set_entry_parts(&mut self, x: i32, y: i32, new_bit: bool, new_data: T) {
        match self.mut_entry(x, y) {
            Some(entry) => {
                entry.bit = new_bit;
                entry.data = new_data;
            }
            None => (),
        };
    }

    /// Sets the contents of the bitmap entry at coordinates `(x, y)`, if it exists.
    pub fn set_entry(&mut self, x: i32, y: i32, new_entry: BitmapEntry<T>) {
        self.mut_entry(x, y).map(|entry| *entry = new_entry);
    }

    /// Rotates the bitmap by 90 degrees clockwise.
    pub fn rotate_clockwise(&mut self) {
        self.rotation = match self.rotation {
            Rotation::By0Degrees => Rotation::By90Degrees,
            Rotation::By90Degrees => Rotation::By180Degrees,
            Rotation::By180Degrees => Rotation::By270Degrees,
            Rotation::By270Degrees => Rotation::By0Degrees,
        };
    }

    /// Rotates the bitmap by 90 degrees counter clockwise.
    pub fn rotate_counter_clockwise(&mut self) {
        self.rotation = match self.rotation {
            Rotation::By0Degrees => Rotation::By270Degrees,
            Rotation::By90Degrees => Rotation::By0Degrees,
            Rotation::By180Degrees => Rotation::By90Degrees,
            Rotation::By270Degrees => Rotation::By180Degrees,
        };
    }

    /// Deletes the provided `rows` from the bitmap by shifting cells down.
    pub fn shift_rows_down(&mut self, rows: &[i32]) {
        let row_set: HashSet<_> = rows.iter().collect();
        let mut row = 0;
        for y in 0..self.height() as i32 {
            if row_set.contains(&y) {
                continue;
            }
            if row != y {
                for x in 0..self.width() as i32 {
                    let entry = self.entry(x, y).unwrap().clone();
                    self.set_entry(x, row, entry);
                }
            }
            row += 1;
        }
        for y in row..self.height() as i32 {
            for x in 0..self.width() as i32 {
                self.set_entry(x, y, Default::default());
            }
        }
    }

    /// Returns the overlapping contents of the bitmap and a mask at the provided offset.
    pub fn query_bits(&self, mask: &Bitmap<T>, x_offset: i32, y_offset: i32) -> BitQueryResult {
        let mut result = BitQueryResult::empty();
        for mask_x in 0..mask.width() as i32 {
            for mask_y in 0..mask.height() as i32 {
                let bit_pair = (self.bit(mask_x + x_offset, mask_y + y_offset),
                                mask.bit(mask_x, mask_y).unwrap());
                result.insert(match bit_pair {
                    (None, false) => MASK_UNSET_OUTSIDE,
                    (None, true) => MASK_SET_OUTSIDE,
                    (Some(false), false) => NONE_SET,
                    (Some(false), true) => MASK_SET,
                    (Some(true), false) => BASE_SET,
                    (Some(true), true) => BASE_AND_MASK_SET,
                });
            }
        }
        result
    }

    /// Blits the bitmap with the mask at the provided offset using the `op` operator.
    pub fn blit(&mut self, mask: &Bitmap<T>, x_offset: i32, y_offset: i32, op: BlitOp) {
        for mask_x in 0..mask.width() as i32 {
            for mask_y in 0..mask.height() as i32 {
                let base_entry = self.mut_entry(mask_x + x_offset, mask_y + y_offset);
                if base_entry.is_none() {
                    continue;
                }
                let mut base_entry = base_entry.unwrap();
                let mask_entry = mask.entry(mask_x, mask_y).unwrap();

                let new_bit = match op {
                    BlitOp::MaskOn => base_entry.bit || mask_entry.bit,
                    BlitOp::MaskOff => base_entry.bit && !mask_entry.bit,
                };
                let overwrite_data = match op {
                    BlitOp::MaskOn => mask_entry.bit,
                    BlitOp::MaskOff => false,
                };

                base_entry.bit = new_bit;
                if overwrite_data {
                    base_entry.data = mask_entry.data.clone();
                }
            }
        }
    }
}
