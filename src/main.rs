extern crate log4rs;
extern crate rand;
extern crate tetris;

use std::fs;
use std::io;
use std::path::PathBuf;
use tetris::game::Game;

static DATA_DIR_NAME: &'static str = "data";

/// Attempts to find and return a directory that contains the project's non-code data.
fn get_data_dir() -> Result<PathBuf, io::Error> {
    let mut current_dir = try!(std::env::current_exe());
    while current_dir.file_name() != None {
        let candidate_dir = current_dir.join(DATA_DIR_NAME);
        match fs::metadata(&candidate_dir) {
            Ok(metadata) => {
                if metadata.is_dir() {
                    return Ok(candidate_dir);
                }
            }
            Err(_) => (),
        }
        current_dir.pop();
    }
    return Err(io::Error::new(io::ErrorKind::NotFound, "Data directory not found"));
}

/// Entry point.
fn main() {
    let data_dir = get_data_dir().unwrap();
    println!("Using \"{}\" as data directory.",
             data_dir.to_str().unwrap());
    log4rs::init_file(data_dir.join("log4rs.yml"), Default::default()).unwrap();

    let mut rng = rand::thread_rng();
    unsafe {
        let mut game = Game::new(&data_dir);
        game.run(&mut rng);
    }
}
