extern crate gl;
extern crate nalgebra as na;
extern crate rand;
extern crate sdl2;

use colors::ColorProvider;
use commands::{CommandGroup, InputInterpreter};
use fonts::FontProvider;
use model_vao::ModelVAO;
use self::gl::types::*;
use self::na::Diagonal;
use self::na::ToHomogeneous;
use self::sdl2::EventPump;
use self::sdl2::event::Event;
use self::sdl2::video::{GLContext, Window};
use std::collections::HashMap;
use std::path::Path;
use std::time::{Duration, SystemTime};
use shaders::{ShaderProgram, ShaderProvider};
use tetris::{BlockKind, BlockState, GameState, Tetris};
use textures::{BitmapTexture, TextTexture};

/// Width of the game window in pixels.
const WINDOW_WIDTH: u32 = 800;
/// Height of the game window in pixels.
const WINDOW_HEIGHT: u32 = 800;
/// Game window aspect ratio.
const WINDOW_ASPECT_RATIO: f32 = WINDOW_WIDTH as f32 / WINDOW_HEIGHT as f32;
/// Game window title.
const WINDOW_TITLE: &'static str = "Tetris";

/// Height of the game window in blocks.
const WINDOW_BLOCK_HEIGHT: u32 = 22;
/// Height of a tetromino block in pixels.
const BLOCK_PIXEL_HEIGHT: u32 = (WINDOW_HEIGHT as f32 / WINDOW_BLOCK_HEIGHT as f32 + 1.0) as u32;

/// OpenGL context version.
const GL_CONTEXT_VERSION: (u8, u8) = (2, 1);
/// Number of state updates per second.
const TICKS_PER_SECOND: u32 = 60;

/// Handles any GL errors.
unsafe fn handle_gl_errors() {
    let error = gl::GetError();
    if error == gl::NO_ERROR {
        return;
    }
    error!("GL error: {}",
           match error {
               gl::INVALID_ENUM => "GL_INVALID_ENUM. An unacceptable value is specified for an enumerated argument.",
               gl::INVALID_VALUE => "GL_INVALID_VALUE. A numeric argument is out of range.",
               gl::INVALID_OPERATION => {
                   "GL_INVALID_OPERATION. The specified operation is not allowed in the current \
                    state."
               }
               gl::INVALID_FRAMEBUFFER_OPERATION => "GL_INVALID_FRAMEBUFFER_OPERATION. The framebuffer object is not complete.",
               gl::OUT_OF_MEMORY => "GL_OUT_OF_MEMORY. There is not enough memory left to execute the command.",
               gl::STACK_UNDERFLOW => {
                   "GL_STACK_UNDERFLOW. An attempt has been made to perform an operation that \
                    would cause an internal stack to underflow."
               }
               gl::STACK_OVERFLOW => {
                   "GL_STACK_OVERFLOW. An attempt has been made to perform an operation that would \
                    cause an internal stack to overflow."
               }
               _ => "Unknown GL error.",
           });
}

/// Model-view-projection matrix.
struct MVPTransform {
    pub model_transform: na::Matrix4<f32>,
    pub view_transform: na::Matrix4<f32>,
    pub projection: na::Matrix4<f32>,
}

impl MVPTransform {
    /// Creates a new transformation.
    fn new() -> MVPTransform {
        MVPTransform {
            model_transform: na::one(),
            view_transform: na::one(),
            projection: na::one(),
        }
    }

    /// Sets the uniform at the provided `location` to the MVP transformation matrix.
    unsafe fn set_uniform(&self, location: GLint) {
        let full_transform = self.projection * self.view_transform * self.model_transform;
        gl::UniformMatrix4fv(location,
                             1,
                             gl::FALSE,
                             full_transform.as_ref().as_ptr() as *const GLfloat);
    }
}

/// Returns a model transformation matrix scales an object along X and Y axes by factors `scale_x`
/// and `scale_y` and that positions it at coordinates `(x, y)` and
fn simple_model_transform(x: f32, y: f32, scale_x: f32, scale_y: f32) -> na::Matrix4<f32> {
    let scale = na::Matrix4::from_diagonal(&na::Vector4::new(scale_x, scale_y, 1.0, 1.0));
    let translation = na::Isometry3::new(na::Vector3::new(x, y, 0.0), na::zero()).to_homogeneous();
    translation * scale
}

/// Horizontal alignment of an object.
enum HorizontalAlignment {
    Left,
    Center,
    Right,
}

/// Identifiers for every text texture.
#[derive(Eq, Hash, PartialEq)]
enum TextTextureId {
    GameOver,
    Hold,
    Level,
    LevelNumber,
    LineCount,
    Lines,
    Next,
    Paused,
    PressToRestart,
    PressToStart,
    Tetris,
}

/// The full game state.
pub struct Game {
    window: Window,
    _gl_context: GLContext,
    event_pump: EventPump,
    input_interpreter: InputInterpreter,

    tetris: Tetris,
    color_provider: ColorProvider,

    block_model: ModelVAO,
    quad_model: ModelVAO,
    blank_texture: BitmapTexture,
    text_textures: HashMap<TextTextureId, TextTexture>,

    mvp_transform: MVPTransform,
    block_shaders: ShaderProgram,
    bitmap_shaders: ShaderProgram,
}

impl Game {
    /// Returns new game state, whose data is read from directory `data_dir`. Dies if
    /// initialization fails.
    pub unsafe fn new(data_dir: &Path) -> Game {
        let sdl_context = sdl2::init().expect("SDL context creation");
        let sdl_video = sdl_context.video().expect("SDL video subsystem creation");
        {
            let (version_major, version_minor) = GL_CONTEXT_VERSION;
            let gl_attr = sdl_video.gl_attr();
            gl_attr.set_double_buffer(true);
            gl_attr.set_stereo(false);
            gl_attr.set_context_version(version_major, version_minor);
            gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        }

        let window = sdl_video.window(WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT)
            .position_centered()
            .opengl()
            .build()
            .expect("Window creation");
        let gl_context = window.gl_create_context().expect("OpenGL context creation");
        gl::load_with(|s| sdl_video.gl_get_proc_address(s) as *const _);

        let event_pump = sdl_context.event_pump().expect("Event pump creation");

        let mut group_bindings = HashMap::new();
        group_bindings.insert(CommandGroup::HardDrop, "Up");
        group_bindings.insert(CommandGroup::Hold, "C");
        group_bindings.insert(CommandGroup::MoveLeft, "Left");
        group_bindings.insert(CommandGroup::MoveRight, "Right");
        group_bindings.insert(CommandGroup::RotateClockwise, "X");
        group_bindings.insert(CommandGroup::RotateCounterClockwise, "Z");
        group_bindings.insert(CommandGroup::SoftDrop, "Down");
        group_bindings.insert(CommandGroup::Start, "Return");
        let input_interpreter =
            InputInterpreter::new(group_bindings).expect("Input event interpreter creation");

        let tetris = Tetris::new();
        let color_provider = ColorProvider::new();

        let half_block_height = WINDOW_BLOCK_HEIGHT as f32 / 2.0;
        let (x_extents, y_extents) = if WINDOW_WIDTH > WINDOW_HEIGHT {
            (half_block_height * WINDOW_ASPECT_RATIO, half_block_height)
        } else {
            (half_block_height, half_block_height / WINDOW_ASPECT_RATIO)
        };

        let mut mvp_transform = MVPTransform::new();
        mvp_transform.view_transform =
            na::Similarity3::new(na::Vector3::new(tetris.width() as f32 / -2.0,
                                                  tetris.height() as f32 / -2.0,
                                                  0.0),
                                 na::zero(),
                                 1.0)
                    .to_homogeneous();
        mvp_transform.projection =
            na::OrthographicMatrix3::new(-x_extents, x_extents, -y_extents, y_extents, -1.0, 1.0)
                .to_matrix();

        let mut shader_provider =
            ShaderProvider::new(&data_dir.join("shaders")).expect("Shader provider creation");

        // Initialize block model and shaders.
        let block_shaders =
            shader_provider.new_program("basic", "model").expect("Shader program creation");
        block_shaders.use_program();

        let block_model = ModelVAO::from_obj(&data_dir.join("models").join("bezel_block.obj"))
            .expect("Loading block model");
        block_model.bind_vao();

        gl::EnableVertexAttribArray(block_shaders.attribute_location("vertPosition").unwrap());
        block_model.define_vertex_coord_attribute(block_shaders.attribute_location("vertPosition")
                                                      .unwrap(),
                                                  3);
        gl::EnableVertexAttribArray(block_shaders.attribute_location("vertNormal").unwrap());
        block_model.define_normal_attribute(block_shaders.attribute_location("vertNormal")
                                                .unwrap(),
                                            3);

        let light_dir: [f32; 3] = [-1.0, 1.0, -4.0];
        gl::Uniform3fv(block_shaders.uniform_location("lightDir").unwrap(),
                       1,
                       light_dir.as_ptr() as *const GLfloat);

        // Initialize quad model and shaders.
        let bitmap_shaders =
            shader_provider.new_program("basic", "bitmap").expect("Quad shader creation.");
        bitmap_shaders.use_program();

        let quad_model =
            ModelVAO::from_obj(&data_dir.join("models").join("quad.obj")).expect("Loading quad");
        quad_model.bind_vao();

        gl::EnableVertexAttribArray(bitmap_shaders.attribute_location("vertPosition").unwrap());
        quad_model.define_vertex_coord_attribute(bitmap_shaders.attribute_location("vertPosition")
                                                     .unwrap(),
                                                 3);
        gl::EnableVertexAttribArray(bitmap_shaders.attribute_location("vertTexCoords").unwrap());
        quad_model.define_texture_coord_attribute(bitmap_shaders.attribute_location("vertTexCoords")
                                                      .unwrap(),
                                                  2);

        let mut blank_texture = BitmapTexture::new();
        blank_texture.set_contents(1, 1, &[255]);

        // Ideally text textures would be initialized here, but I haven't figured out how to
        // satisfy the borrow checker yet.
        let text_textures = HashMap::new();

        Game {
            window: window,
            _gl_context: gl_context,
            event_pump: event_pump,
            input_interpreter: input_interpreter,

            tetris: tetris,
            color_provider: color_provider,

            block_model: block_model,
            quad_model: quad_model,
            blank_texture: blank_texture,
            text_textures: text_textures,

            block_shaders: block_shaders,
            bitmap_shaders: bitmap_shaders,
            mvp_transform: mvp_transform,
        }
    }

    /// Draws a block with the provided `block_state` at coordinates `(x, y)`. Assumes that block
    /// shader program is in use and block model's VAO is bound.
    unsafe fn draw_block(&mut self, x: f32, y: f32, block_state: &BlockState) {
        self.mvp_transform.model_transform = simple_model_transform(x, y, 1.0, 1.0);
        self.mvp_transform.set_uniform(self.block_shaders.uniform_location("transform").unwrap());

        let color = match block_state.kind {
            BlockKind::Empty => self.color_provider.playfield_background(),
            BlockKind::Normal => self.color_provider.block_color(&block_state.color),
            BlockKind::Shadow => self.color_provider.shadow_color(&block_state.color),
        };
        gl::Uniform4fv(self.block_shaders.uniform_location("color").unwrap(),
                       1,
                       color.as_gl_ptr());
        self.block_model.draw();
    }

    /// Draws the text texture with `texture_id` with provided `height` positioned at coordinates
    /// `(x, y)` and aligned to `alignment`. Assumes that bitmap shader program is in used and quad
    /// model's VAO is bound.
    unsafe fn draw_text(&mut self,
                        texture_id: TextTextureId,
                        height: f32,
                        x: f32,
                        y: f32,
                        alignment: HorizontalAlignment) {
        let texture = self.text_textures.get(&texture_id).unwrap();
        texture.bind();

        let x_offset = match alignment {
            HorizontalAlignment::Left => 0.0,
            HorizontalAlignment::Center => height * texture.aspect_ratio() / 2.0,
            HorizontalAlignment::Right => height * texture.aspect_ratio(),
        };
        let x = x - x_offset;

        self.mvp_transform.model_transform =
            simple_model_transform(x, y, height * texture.aspect_ratio(), height);
        self.mvp_transform.set_uniform(self.bitmap_shaders.uniform_location("transform").unwrap());
        gl::Uniform4fv(self.bitmap_shaders.uniform_location("color").unwrap(),
                       1,
                       self.color_provider.white().as_gl_ptr());
        self.quad_model.draw();
    }

    /// Renders the game's splash screen.
    unsafe fn draw_splash_screen(&mut self) {
        self.bitmap_shaders.use_program();
        self.quad_model.bind_vao();

        let playfield_width = self.tetris.width() as f32;
        let playfield_height = self.tetris.height() as f32;
        self.draw_text(TextTextureId::Tetris,
                       2.0,
                       playfield_width / 2.0,
                       playfield_height / 2.0,
                       HorizontalAlignment::Center);
        self.draw_text(TextTextureId::PressToStart,
                       1.0,
                       playfield_width / 2.0,
                       playfield_height / 2.0 - 1.0,
                       HorizontalAlignment::Center);
    }

    /// Renders the UI.
    unsafe fn draw_ui(&mut self) {
        self.bitmap_shaders.use_program();
        self.quad_model.bind_vao();

        let playfield_width = self.tetris.width() as f32;
        let playfield_height = self.tetris.height() as f32;
        self.draw_text(TextTextureId::Hold,
                       1.0,
                       -1.0,
                       playfield_height - 1.0,
                       HorizontalAlignment::Right);
        self.draw_text(TextTextureId::Level,
                       1.0,
                       -1.0,
                       6.0,
                       HorizontalAlignment::Right);
        self.draw_text(TextTextureId::LevelNumber,
                       2.0,
                       -1.0,
                       4.0,
                       HorizontalAlignment::Right);
        self.draw_text(TextTextureId::Lines,
                       1.0,
                       -1.0,
                       2.0,
                       HorizontalAlignment::Right);
        self.draw_text(TextTextureId::LineCount,
                       2.0,
                       -1.0,
                       0.0,
                       HorizontalAlignment::Right);
        self.draw_text(TextTextureId::Next,
                       1.0,
                       playfield_width + 1.0,
                       playfield_height - 1.0,
                       HorizontalAlignment::Left);
    }

    /// Renders the game's playfield.
    unsafe fn draw_playfield(&mut self) {
        self.block_shaders.use_program();
        self.block_model.bind_vao();

        for x in 0..self.tetris.width() as i32 {
            for y in 0..self.tetris.height() as i32 {
                match self.tetris.block(x, y) {
                    Some(state) => self.draw_block(x as f32, y as f32, &state),
                    None => (),
                };
            }
        }

        if self.tetris.held_piece().is_some() {
            let held_piece = self.tetris
                .held_piece()
                .as_ref()
                .unwrap()
                .clone();
            for x in 0..held_piece.width() as i32 {
                for y in 0..held_piece.height() as i32 {
                    let entry = held_piece.entry(x, y).unwrap();
                    if entry.bit {
                        self.draw_block(x as f32 - 1.0 - held_piece.width() as f32,
                                        y as f32 + 18.5 - held_piece.height() as f32,
                                        &BlockState::new(BlockKind::Normal, entry.data.clone()));
                    }
                }
            }
        }

        let next_piece = self.tetris.next_piece().clone();
        for x in 0..next_piece.width() as i32 {
            for y in 0..next_piece.height() as i32 {
                let entry = next_piece.entry(x, y).unwrap();
                if entry.bit {
                    self.draw_block(x as f32 + 11.0,
                                    y as f32 + 18.5 - next_piece.height() as f32,
                                    &BlockState::new(BlockKind::Normal, entry.data.clone()));
                }
            }
        }
    }

    /// Renders the game over screen.
    unsafe fn draw_game_over(&mut self) {
        self.bitmap_shaders.use_program();
        self.quad_model.bind_vao();

        let playfield_width = self.tetris.width() as f32;
        let playfield_height = self.tetris.height() as f32;

        self.blank_texture.bind();
        self.mvp_transform.model_transform =
            simple_model_transform(0.0, playfield_height / 2.0 - 2.0, playfield_width, 4.5);
        self.mvp_transform.set_uniform(self.bitmap_shaders.uniform_location("transform").unwrap());
        gl::Uniform4fv(self.bitmap_shaders.uniform_location("color").unwrap(),
                       1,
                       self.color_provider
                           .black()
                           .with_alpha(0.5)
                           .as_gl_ptr());
        self.quad_model.draw();

        self.draw_text(TextTextureId::GameOver,
                       2.0,
                       playfield_width / 2.0,
                       playfield_height / 2.0,
                       HorizontalAlignment::Center);
        self.draw_text(TextTextureId::PressToRestart,
                       1.0,
                       playfield_width / 2.0,
                       playfield_height / 2.0 - 1.0,
                       HorizontalAlignment::Center);
    }

    /// Renders the pause screen.
    unsafe fn draw_pause(&mut self) {
        self.bitmap_shaders.use_program();
        self.quad_model.bind_vao();

        let playfield_width = self.tetris.width() as f32;
        self.draw_text(TextTextureId::Paused,
                       1.0,
                       playfield_width + 1.0,
                       0.0,
                       HorizontalAlignment::Left);
    }

    /// Renders the current frame.
    unsafe fn render_frame(&mut self) {
        gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        gl::Clear(gl::COLOR_BUFFER_BIT);

        if self.tetris.state() == &GameState::Start {
            self.draw_splash_screen();
        } else {
            self.draw_ui();
            self.draw_playfield();
            if self.tetris.state() == &GameState::GameOver {
                self.draw_game_over();
            } else if self.tetris.state() == &GameState::Paused {
                self.draw_pause();
            }
        }

        gl::BindVertexArray(0);
        gl::UseProgram(0);
        handle_gl_errors();
    }

    /// Runs the game loop.
    pub unsafe fn run<R: rand::Rng>(&mut self, mut rng: &mut R) {
        let sdl_ttf_context = sdl2::ttf::init().expect("SDL TTF context creation.");
        let mut font_provider = FontProvider::new(sdl_ttf_context);
        font_provider.load_family("sans").unwrap();
        let small_font = font_provider.get_font("sans", BLOCK_PIXEL_HEIGHT as u16).unwrap();
        let large_font = font_provider.get_font("sans", (BLOCK_PIXEL_HEIGHT * 2) as u16).unwrap();

        let start_key = self.input_interpreter
            .bound_keycode(&CommandGroup::Start)
            .unwrap()
            .to_string();
        gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
        self.text_textures.insert(TextTextureId::GameOver,
                                  TextTexture::new_with_contents(&large_font, "GAME OVER")
                                      .unwrap());
        self.text_textures.insert(TextTextureId::Hold,
                                  TextTexture::new_with_contents(&small_font, "HOLD").unwrap());
        self.text_textures.insert(TextTextureId::Level,
                                  TextTexture::new_with_contents(&small_font, "LEVEL").unwrap());
        self.text_textures.insert(TextTextureId::LevelNumber,
                                  TextTexture::new_with_contents(&large_font, "0").unwrap());
        self.text_textures.insert(TextTextureId::LineCount,
                                  TextTexture::new_with_contents(&large_font, "0").unwrap());
        self.text_textures.insert(TextTextureId::Lines,
                                  TextTexture::new_with_contents(&small_font, "LINES").unwrap());
        self.text_textures.insert(TextTextureId::Next,
                                  TextTexture::new_with_contents(&small_font, "NEXT").unwrap());
        self.text_textures.insert(TextTextureId::Paused,
                                  TextTexture::new_with_contents(&small_font, "PAUSED").unwrap());
        self.text_textures
            .insert(TextTextureId::PressToRestart,
                    TextTexture::new_with_contents(&small_font,
                                                   &format!("Press '{}' to play again",
                                                            start_key))
                            .unwrap());
        self.text_textures.insert(TextTextureId::PressToStart,
                                  TextTexture::new_with_contents(&small_font,
                                                                 &format!("Press '{}' to play",
                                                                          start_key))
                                          .unwrap());
        self.text_textures.insert(TextTextureId::Tetris,
                                  TextTexture::new_with_contents(&large_font, "T E T R I S")
                                      .unwrap());

        gl::Enable(gl::BLEND);
        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

        let step_length = Duration::new(0, 1000000000 / TICKS_PER_SECOND);
        let mut system_time = SystemTime::now();
        let mut prev_elapsed = Duration::new(0, 0);
        let mut time_accumulator = Duration::new(0, 0);

        'main_loop: loop {
            for event in self.event_pump.poll_iter() {
                if self.input_interpreter.record_event(&event) {
                    continue;
                }
                match event {
                    Event::Quit { .. } => break 'main_loop,
                    _ => (),
                }
            }

            let current_elapsed = match system_time.elapsed() {
                Ok(elapsed) => elapsed,
                Err(_) => {
                    system_time = SystemTime::now();
                    prev_elapsed
                }
            };
            time_accumulator += current_elapsed - prev_elapsed;
            prev_elapsed = current_elapsed;
            while time_accumulator >= step_length {
                time_accumulator -= step_length;
                self.tetris.advance(&mut rng, &self.input_interpreter.get_commands_and_advance());
            }

            if self.tetris.level_updates() > 0 {
                let texture = self.text_textures.get_mut(&TextTextureId::LevelNumber).unwrap();
                texture.set_contents(&large_font, &self.tetris.level().to_string())
                    .expect("Level number should render");
            }
            if self.tetris.lines_cleared_updates() > 0 {
                let texture = self.text_textures.get_mut(&TextTextureId::LineCount).unwrap();
                texture.set_contents(&large_font, &self.tetris.lines_cleared().to_string())
                    .expect("Line counter should render");
            }

            self.render_frame();
            self.window.gl_swap_window();
        }
    }
}
