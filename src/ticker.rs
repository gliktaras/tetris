#![allow(dead_code)]

/// A ticker that fires at a fixed period.
pub struct PeriodicTicker {
    period: u32,
    tick: u32,
    running: bool,
}

impl PeriodicTicker {
    /// Returns a new ticker with the provided `period`.
    pub fn new(period: u32) -> PeriodicTicker {
        PeriodicTicker {
            period: period,
            tick: 0,
            running: false,
        }
    }

    /// Returns the ticker's period.
    pub fn period(&self) -> u32 {
        self.period
    }

    /// Sets a new period and resets the tickets.
    pub fn set_period(&mut self, new_period: u32) {
        self.period = new_period;
        self.tick = 0;
    }

    /// Resets the ticker.
    pub fn reset(&mut self) {
        self.tick = 0;
    }

    /// Forces the ticker to fire immediately.
    pub fn fire(&mut self) {
        self.tick = self.period;
    }

    /// Resumes the ticker.
    pub fn resume(&mut self) {
        self.running = true;
    }

    /// Pauses the ticker.
    pub fn pause(&mut self) {
        self.running = false;
    }

    /// Fires the ticker and resumes it.
    pub fn fire_and_resume(&mut self) {
        self.fire();
        self.resume();
    }

    /// Advances the ticker and returns if it fired.
    pub fn advance(&mut self) -> bool {
        if !self.running {
            return false;
        }
        self.tick = if self.tick >= self.period {
            0
        } else {
            self.tick + 1
        };
        self.tick == 0
    }
}

/// A ticker that counts the number of ticks since the last query.
pub struct TickCounter {
    ticks: u32,
}

impl TickCounter {
    /// Returns a new tick counter with the provided initial tick number.
    pub fn new(init_ticks: u32) -> TickCounter {
        TickCounter { ticks: init_ticks }
    }

    /// Records a new tick.
    pub fn tick(&mut self) {
        self.ticks += 1;
    }

    /// Returns the number of ticks since the last query.
    pub fn new_ticks(&mut self) -> u32 {
        let ticks = self.ticks;
        self.ticks = 0;
        ticks
    }

    /// Sets the number of recorded ticks to `new_ticks`.
    pub fn set(&mut self, new_ticks: u32) {
        self.ticks = new_ticks;
    }
}
