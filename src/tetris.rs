extern crate rand;

use bitmap::{BASE_AND_MASK_SET, BASE_SET, Bitmap, BlitOp, MASK_SET_OUTSIDE, NONE_SET};
use commands::Command;
use std::cmp;
use std::mem;
use tetrominoes::{BlockColor, TetrominoFactory};
use ticker::{PeriodicTicker, TickCounter};

/// Piece falling speeds for every difficulty level.
static LEVEL_SPEED: [u32; 20] = [60, 48, 39, 31, 25, 20, 16, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3,
                                 2, 1];
/// Number of lines to clear to advance a level.
static LINES_PER_LEVEL: u32 = 30;
/// Height of a tetris playfield.
static PLAYFIELD_HEIGHT: usize = 20;
/// Width of a tetris playfield.
static PLAYFIELD_WIDTH: usize = 10;

/// Possible playfield block kinds.
#[derive(Eq, PartialEq)]
pub enum BlockKind {
    Empty,
    Normal,
    Shadow,
}

/// State of an individual playfield block.
pub struct BlockState {
    pub kind: BlockKind,
    pub color: BlockColor,
}

impl BlockState {
    /// Returns a new block with the provided state.
    pub fn new(kind: BlockKind, color: BlockColor) -> BlockState {
        BlockState {
            kind: kind,
            color: color,
        }
    }
}

/// Possible high-level game states.
#[derive(Eq, PartialEq)]
pub enum GameState {
    GameOver,
    InPlay,
    Paused,
    Start,
}

/// An instance of a tetris game.
pub struct Tetris {
    tetromino_factory: TetrominoFactory,
    line: Bitmap<BlockColor>,

    playfield: Bitmap<BlockColor>,
    piece: Bitmap<BlockColor>,
    next_piece: Bitmap<BlockColor>,
    piece_x: i32,
    piece_y: i32,
    shadow_y: i32,
    has_piece: bool,
    fall_ticker: PeriodicTicker,
    held_piece: Option<Bitmap<BlockColor>>,
    can_hold: bool,
    state: GameState,

    soft_drop_ticker: PeriodicTicker,
    move_left_ticker: PeriodicTicker,
    move_right_ticker: PeriodicTicker,

    level: u32,
    level_updates: TickCounter,
    lines_cleared: u32,
    lines_cleared_updates: TickCounter,
}

impl Tetris {
    /// Returns a new instance of tetris.
    pub fn new() -> Tetris {
        let tetromino_factory = TetrominoFactory::new();
        Tetris {
            tetromino_factory: tetromino_factory,
            line: Bitmap::new(PLAYFIELD_WIDTH, 1),
            playfield: Bitmap::new(PLAYFIELD_WIDTH, PLAYFIELD_HEIGHT + 4),
            piece: Bitmap::new(1, 1),
            next_piece: Bitmap::new(1, 1),
            piece_x: 0,
            piece_y: 0,
            shadow_y: 0,
            has_piece: false,
            fall_ticker: PeriodicTicker::new(0),
            held_piece: None,
            can_hold: true,
            state: GameState::Start,
            soft_drop_ticker: PeriodicTicker::new(2),
            move_left_ticker: PeriodicTicker::new(6),
            move_right_ticker: PeriodicTicker::new(6),
            level: 0,
            level_updates: TickCounter::new(1),
            lines_cleared: 0,
            lines_cleared_updates: TickCounter::new(1),
        }
    }

    /// Returns whether `piece` fits in the playfield at offset `(x, y)` without overlapping the
    /// existing blocks.
    fn can_fit_piece(&self, piece: &Bitmap<BlockColor>, x: i32, y: i32) -> bool {
        let query_bits = self.playfield.query_bits(piece, x, y);
        !query_bits.contains(BASE_AND_MASK_SET) && !query_bits.contains(MASK_SET_OUTSIDE)
    }

    /// Updates the shadow piece to match the current active piece.
    fn update_shadow_piece(&mut self) {
        self.shadow_y = self.piece_y;
        while self.can_fit_piece(&self.piece, self.piece_x, self.shadow_y - 1) {
            self.shadow_y -= 1;
        }
    }

    /// Positions the active piece at the top of the playfield.
    fn position_active_piece(&mut self) {
        let new_piece_x = ((self.width() - self.piece.width()) / 2) as i32;
        self.piece_x = new_piece_x;

        let mut new_piece_y = (self.height() - self.piece.height()) as i32;
        let query_bits = self.piece.query_bits(&self.line, 0, self.piece.height() as i32 - 1);
        if !query_bits.contains(BASE_SET) {
            new_piece_y += 1;
        }
        self.piece_y = new_piece_y;

        self.has_piece = true;
        self.fall_ticker.reset();
        self.update_shadow_piece();
    }

    /// Spawns the next piece on the playfield.
    fn spawn_next_piece<R: rand::Rng>(&mut self, rng: &mut R) {
        self.piece = mem::replace(&mut self.next_piece, self.tetromino_factory.next_piece(rng));
        self.position_active_piece();
        self.can_hold = true;
    }

    /// Moves the active piece one column to the left.
    fn move_piece_left(&mut self) {
        if !self.has_piece {
            return;
        }
        if self.can_fit_piece(&self.piece, self.piece_x - 1, self.piece_y) {
            self.piece_x -= 1;
            self.update_shadow_piece();
        }
    }

    /// Moves the active piece one column to the right.
    fn move_piece_right(&mut self) {
        if !self.has_piece {
            return;
        }
        if self.can_fit_piece(&self.piece, self.piece_x + 1, self.piece_y) {
            self.piece_x += 1;
            self.update_shadow_piece();
        }
    }

    /// Attempts to position the active piece after a rotation, including kicks. Returns whether it
    /// was successful.
    fn try_fit_piece_after_rotation(&mut self) -> bool {
        if self.can_fit_piece(&self.piece, self.piece_x, self.piece_y) {
            self.update_shadow_piece();
        } else if self.can_fit_piece(&self.piece, self.piece_x - 1, self.piece_y) {
            self.piece_x -= 1;
            self.update_shadow_piece();
        } else if self.can_fit_piece(&self.piece, self.piece_x + 1, self.piece_y) {
            self.piece_x += 1;
            self.update_shadow_piece();
        } else if self.can_fit_piece(&self.piece, self.piece_x, self.piece_y + 1) {
            self.piece_y += 1;
            self.update_shadow_piece();
        } else {
            return false;
        }
        true
    }

    /// Rotates the active piece clockwise.
    fn rotate_piece_clockwise(&mut self) {
        if !self.has_piece {
            return;
        }
        self.piece.rotate_clockwise();
        if !self.try_fit_piece_after_rotation() {
            self.piece.rotate_counter_clockwise();
        }
    }

    /// Rotates the active piece counter clockwise.
    fn rotate_piece_counter_clockwise(&mut self) {
        if !self.has_piece {
            return;
        }
        self.piece.rotate_counter_clockwise();
        if !self.try_fit_piece_after_rotation() {
            self.piece.rotate_clockwise();
        }
    }

    /// Places the active piece on the playfield.
    fn place_piece(&mut self) {
        self.playfield.blit(&self.piece, self.piece_x, self.piece_y, BlitOp::MaskOn);
        self.has_piece = false;
    }

    /// Hard drops the active piece.
    fn hard_drop_piece(&mut self) {
        while self.can_fit_piece(&self.piece, self.piece_x, self.piece_y - 1) {
            self.piece_y -= 1;
        }
        self.place_piece();
    }

    /// Soft drops the active piece.
    fn soft_drop_piece(&mut self) {
        if self.can_fit_piece(&self.piece, self.piece_x, self.piece_y - 1) {
            self.fall_ticker.reset();
            self.piece_y -= 1;
        } else {
            self.place_piece();
        }
    }

    /// Holds the active piece.
    fn hold_piece<R: rand::Rng>(&mut self, rng: &mut R) {
        if !self.can_hold {
            return;
        }

        if self.held_piece.is_some() {
            mem::swap(&mut self.piece, self.held_piece.as_mut().unwrap());
            self.position_active_piece();
        } else {
            self.held_piece = Some(self.piece.clone());
            self.spawn_next_piece(rng);
        }
        self.can_hold = false;
    }

    /// Sets the current difficulty level.
    fn set_level(&mut self, level: u32) {
        self.level = level;
        self.level_updates.tick();
        let level_index = cmp::min(level, LEVEL_SPEED.len() as u32) as usize - 1;
        self.fall_ticker.set_period(LEVEL_SPEED[level_index]);
    }

    /// (Re)starts a game.
    fn start<R: rand::Rng>(&mut self, rng: &mut R) {
        self.playfield = Bitmap::new(PLAYFIELD_WIDTH, PLAYFIELD_HEIGHT + 4);
        self.tetromino_factory.reset(rng);
        self.next_piece = self.tetromino_factory.next_piece(rng);
        self.spawn_next_piece(rng);
        self.set_fall_speed(60);
        self.fall_ticker.resume();
        self.set_level(1);
        self.lines_cleared = 0;
        self.lines_cleared_updates.set(1);
    }

    /// Returns the width of the playfield.
    pub fn width(&self) -> usize {
        PLAYFIELD_WIDTH
    }

    /// Returns the height of the playfield.
    pub fn height(&self) -> usize {
        PLAYFIELD_HEIGHT
    }

    /// Returns the current game state.
    pub fn state(&self) -> &GameState {
        &self.state
    }

    /// Returns the state of the block at coordinates `(x, y)`.
    pub fn block(&self, x: i32, y: i32) -> Option<BlockState> {
        match self.playfield.entry(x, y) {
            Some(entry) => {
                if entry.bit {
                    return Some(BlockState::new(BlockKind::Normal, entry.data.clone()));
                }
            }
            None => (),
        }
        if self.has_piece {
            match self.piece.entry(x - self.piece_x, y - self.piece_y) {
                Some(entry) => {
                    if entry.bit {
                        return Some(BlockState::new(BlockKind::Normal, entry.data.clone()));
                    }
                }
                None => (),
            }
            match self.piece.entry(x - self.piece_x, y - self.shadow_y) {
                Some(entry) => {
                    if entry.bit {
                        return Some(BlockState::new(BlockKind::Shadow, entry.data.clone()));
                    }
                }
                None => (),
            }
        }
        if self.playfield.entry(x, y).is_some() && x < self.width() as i32 &&
           y < self.height() as i32 {
            return Some(BlockState::new(BlockKind::Empty, Default::default()));
        }
        None
    }

    /// Returns the next piece.
    pub fn next_piece(&self) -> &Bitmap<BlockColor> {
        &self.next_piece
    }

    /// Returns the current hold piece.
    pub fn held_piece(&self) -> &Option<Bitmap<BlockColor>> {
        &self.held_piece
    }

    /// Sets the piece's fall speed in ticks per row.
    pub fn set_fall_speed(&mut self, ticks_per_row: u32) {
        self.fall_ticker.set_period(ticks_per_row);
    }

    /// Returns the current speed level.
    pub fn level(&self) -> u32 {
        self.level
    }

    /// Returns the number of level changes since the last query.
    pub fn level_updates(&mut self) -> u32 {
        self.level_updates.new_ticks()
    }

    /// Returns the number of lines cleared.
    pub fn lines_cleared(&self) -> u32 {
        self.lines_cleared
    }

    /// Returns the number of times lines cleared updated since the last query.
    pub fn lines_cleared_updates(&mut self) -> u32 {
        self.lines_cleared_updates.new_ticks()
    }

    /// Advances game state by one tick when game is in state Start.
    fn advance_start<R: rand::Rng>(&mut self, rng: &mut R, commands: &Vec<Command>) {
        for command in commands {
            match command {
                &Command::Start => {
                    self.start(rng);
                    self.state = GameState::InPlay;
                }
                _ => (),
            }
        }
    }

    /// Advances game state by one tick when game is in state InPlay.
    fn advance_in_play<R: rand::Rng>(&mut self, rng: &mut R, commands: &Vec<Command>) {
        for command in commands {
            match command {
                &Command::HardDrop => {
                    self.hard_drop_piece();
                }
                &Command::Hold => {
                    self.hold_piece(rng);
                }
                &Command::MoveLeftOff => {
                    self.move_left_ticker.pause();
                }
                &Command::MoveLeftOn => {
                    self.move_left_ticker.fire_and_resume();
                }
                &Command::MoveRightOff => {
                    self.move_right_ticker.pause();
                }
                &Command::MoveRightOn => {
                    self.move_right_ticker.fire_and_resume();
                }
                &Command::RotateClockwise => {
                    self.rotate_piece_clockwise();
                }
                &Command::RotateCounterClockwise => {
                    self.rotate_piece_counter_clockwise();
                }
                &Command::SoftDropOff => {
                    self.soft_drop_ticker.pause();
                }
                &Command::SoftDropOn => {
                    self.soft_drop_ticker.fire_and_resume();
                }
                &Command::Start => {
                    self.state = GameState::Paused;
                }
            }
        }

        if self.soft_drop_ticker.advance() {
            self.soft_drop_piece();
        }
        if self.move_left_ticker.advance() {
            self.move_piece_left();
        }
        if self.move_right_ticker.advance() {
            self.move_piece_right();
        }

        if !self.has_piece {
            let mut filled_rows = Vec::new();
            for y in 0..self.height() as i32 {
                if !self.playfield.query_bits(&self.line, 0, y).contains(NONE_SET) {
                    filled_rows.push(y);
                }
            }
            if !filled_rows.is_empty() {
                self.playfield.shift_rows_down(filled_rows.as_slice());
                self.lines_cleared += filled_rows.len() as u32;
                self.lines_cleared_updates.tick();
                if self.lines_cleared >= LINES_PER_LEVEL * self.level {
                    let new_level = self.lines_cleared / LINES_PER_LEVEL + 1;
                    self.set_level(new_level);
                }
            }

            self.spawn_next_piece(rng);
            if !self.can_fit_piece(&self.piece, self.piece_x, self.piece_y) {
                self.state = GameState::GameOver;
            }
        } else if self.fall_ticker.advance() {
            self.soft_drop_piece();
        }
    }

    /// Advances game state by one tick when game is in state Paused.
    fn advance_paused(&mut self, commands: &Vec<Command>) {
        for command in commands {
            match command {
                &Command::Start => {
                    self.state = GameState::InPlay;
                }
                _ => (),
            }
        }
    }

    /// Advances game state by one tick when game is in state GameOver.
    fn advance_game_over<R: rand::Rng>(&mut self, rng: &mut R, commands: &Vec<Command>) {
        for command in commands {
            match command {
                &Command::Start => {
                    self.start(rng);
                    self.state = GameState::InPlay;
                }
                _ => (),
            }
        }
    }

    /// Applies the provided `commands` and advances the game's state by one tick.
    pub fn advance<R: rand::Rng>(&mut self, rng: &mut R, commands: &Vec<Command>) {
        match self.state {
            GameState::GameOver => self.advance_game_over(rng, commands),
            GameState::InPlay => self.advance_in_play(rng, commands),
            GameState::Paused => self.advance_paused(commands),
            GameState::Start => self.advance_start(rng, commands),
        }
    }
}
