#version 130

in vec2 fragTexCoords;

uniform vec4 color;
uniform sampler2D bitmap;

void main() {
    gl_FragColor = vec4(color.xyz, color.w * texture(bitmap, fragTexCoords).r);
}
