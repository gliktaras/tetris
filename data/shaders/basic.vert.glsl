#version 130

in vec3 vertPosition;
in vec3 vertNormal;
in vec2 vertTexCoords;

out vec3 fragNormal;
out vec2 fragTexCoords;

uniform mat4 transform;

void main() {
    gl_Position = transform * vec4(vertPosition, 1.0);
    fragNormal = vertNormal;
    fragTexCoords = vec2(vertTexCoords.x, 1.0 - vertTexCoords.y);
}
