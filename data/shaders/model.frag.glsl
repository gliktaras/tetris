#version 130

in vec3 fragNormal;

uniform vec4 color;
uniform vec3 lightDir;

void main() {
    gl_FragColor = vec4(color.xyz * max(dot(normalize(fragNormal), normalize(lightDir)), 0.0), color.w);
}
